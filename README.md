# Fichiers de d�claration pour debardeur et Salvatore

## Debardeur

### Projets sur git.spip.net

Il suffit de poser un tag pour que le debardeur g�n�re une archive et que le plugin soit recens� sur plugins.spip.net.
Rien � d�clarer ici

### Projets externes

Pour les projets d�velopp�s sur d'autre plateformes git (github, gitlab?), il faut d�clarer le projet dans `archivelist-externals.txt`

L� encore, une fois le projet d�clar� il suffit de poser un tag pour d�clencher la g�n�ration d'un nouveau zip et la mise � jour dans plugins.spip.net

### Projets historiques SVN

Pendant une p�riode de transition les projets sont encore zipp�s depuis SVN par smart-paquet et recens�s sur plugins.spip.net

La d�claration se fait via les 2 fichiers `legacy/archivelist.txt` et `legacy/archivelist_grenier.txt`
Id�alement on ne devrait plus rien y ajouter et ne faire que supprimer des lignes au fur et � mesure que l'on pose des tags sur les projets git.


## Salvatore

Le fichier `traductions.txt` est utilis� par Salvatore pour savoir quels projets il doit traduire, et quels sont les modules de langue concern�s.
Il n'y a pas de m�canisme automatique pour le moment, il faut express�ment d�clarer un projet dans ce fichier pour qu'il soit pris en compte par Salvatore, m�me s�il est d�velopp� sur git.spip.net

