# INFORMATIONS SUR LE DEPOT
# -------------------------
# @titre = SPIP-Zone - Core
# @descriptif = Ce dépôt ne contient que les plugins du Core !
# @type = svn
# @url_serveur = svn://zone.spip.org/spip-zone
# @url_brouteur = https://zone.spip.org/trac/spip-zone/browser
# @url_archives = https://files.spip.net/core
# @url_commits = https://zone.spip.org/trac/spip-zone/log/@src_archive@?format=rss&limit=10&mode=stop_on_copy

# FORMAT DES DECLARATIONS
# -----------------------
# Format des lignes , 2 ou 3 parametres separes par des ";" :
# arborescence_svn;nom_archive;repertoire_dans_archive
# - arborescence_svn = l'arborescence complete a partir de la racine du svn
# - nom_archive = comment va s'appeler l'archive (sans extension .zip)
#   defaut: le dernier morceau de arborescence_svn
# - repertoire_dans_archive = ou vont etre places les fichiers a l'interieur de l'archive
#   defaut: nom_archive
#
# Rem : l'arborescence_svn pouvait etre suivie de :rrrr ou rrrr etait une revision pour fixer un paquet. Cette
# declaration est devenue obsolete, il faut preferer la creation d'un tag svn : 
# svn cp -rXXXX arborescence_svn tags/nom_du_zip et indiquer tags/nom_du_zip dans ce fichier

# LISTE DES PAQUETS DU DEPOT
# ---------------------------------------------------------------------
#
# Extensions du core
#
#

# SPIP 2.1
_core_/branches/spip-2.1/plugins/compresseur;compresseur_21
_core_/branches/spip-2.1/plugins/filtres_images;filtres_images_21
_core_/branches/spip-2.1/plugins/msie_compat;msie_compat_21
_core_/branches/spip-2.1/plugins/safehtml;safehtml_21
_core_/branches/spip-2.1/plugins/porte_plume;porte_plume_21
_core_/branches/spip-2.1/plugins/vertebres;vertebres_21

# SPIP 3.0
_core_/branches/spip-3.0/plugins/breves;breves_30
_core_/branches/spip-3.0/plugins/compagnon;compagnon_30
_core_/branches/spip-3.0/plugins/compresseur;compresseur_30
_core_/branches/spip-3.0/plugins/dev;dev_30
_core_/branches/spip-3.0/plugins/dist;dist_30
_core_/branches/spip-3.0/plugins/dump;dump_30
_core_/branches/spip-3.0/plugins/filtres_images;filtres_images_30
_core_/branches/spip-3.0/plugins/forum;forum_30
_core_/branches/spip-3.0/plugins/grenier;grenier_30
_core_/branches/spip-3.0/plugins/jquery_ui;jquery_ui_30
_core_/branches/spip-3.0/plugins/mediabox;mediabox_30
_core_/branches/spip-3.0/plugins/medias;medias_30
_core_/branches/spip-3.0/plugins/mots;mots_30
_core_/branches/spip-3.0/plugins/msie_compat;msie_compat_30
_core_/branches/spip-3.0/plugins/organiseur;organiseur_30
_core_/branches/spip-3.0/plugins/petitions;petitions_30
_core_/branches/spip-3.0/plugins/porte_plume;porte_plume_30
_core_/branches/spip-3.0/plugins/revisions;revisions_30
_core_/branches/spip-3.0/plugins/safehtml;safehtml_30
_core_/branches/spip-3.0/plugins/sites;sites_30
_core_/branches/spip-3.0/plugins/squelettes_par_rubrique;squelettes_par_rubrique_30
_core_/branches/spip-3.0/plugins/statistiques;statistiques_30
_core_/branches/spip-3.0/plugins/svp;svp_30
_core_/branches/spip-3.0/plugins/textwheel;textwheel_30
_core_/branches/spip-3.0/plugins/themes;themes_30
_core_/branches/spip-3.0/plugins/urls_etendues;urls_etendues_30
_core_/branches/spip-3.0/plugins/vertebres;vertebres_30

# SPIP 3.1
_core_/branches/spip-3.1/plugins/breves;breves_31
_core_/branches/spip-3.1/plugins/compagnon;compagnon_31
_core_/branches/spip-3.1/plugins/compresseur;compresseur_31
_core_/branches/spip-3.1/plugins/dev;dev_31
_core_/branches/spip-3.1/plugins/dist;dist_31
_core_/branches/spip-3.1/plugins/dump;dump_31
_core_/branches/spip-3.1/plugins/filtres_images;filtres_images_31
_core_/branches/spip-3.1/plugins/forum;forum_31
_core_/branches/spip-3.1/plugins/grenier;grenier_31
_core_/branches/spip-3.1/plugins/jquery_ui;jquery_ui_31
_core_/branches/spip-3.1/plugins/mediabox;mediabox_31
_core_/branches/spip-3.1/plugins/medias;medias_31
_core_/branches/spip-3.1/plugins/mots;mots_31
_core_/branches/spip-3.1/plugins/msie_compat;msie_compat_31
_core_/branches/spip-3.1/plugins/organiseur;organiseur_31
_core_/branches/spip-3.1/plugins/petitions;petitions_31
_core_/branches/spip-3.1/plugins/plan;plan_31
_core_/branches/spip-3.1/plugins/porte_plume;porte_plume_31
_core_/branches/spip-3.1/plugins/revisions;revisions_31
_core_/branches/spip-3.1/plugins/safehtml;safehtml_31
_core_/branches/spip-3.1/plugins/sites;sites_31
_core_/branches/spip-3.1/plugins/squelettes_par_rubrique;squelettes_par_rubrique_31
_core_/branches/spip-3.1/plugins/statistiques;statistiques_31
_core_/branches/spip-3.1/plugins/svp;svp_31
_core_/branches/spip-3.1/plugins/textwheel;textwheel_31
_core_/branches/spip-3.1/plugins/themes;themes_31
_core_/branches/spip-3.1/plugins/urls_etendues;urls_etendues_31
_core_/branches/spip-3.1/plugins/vertebres;vertebres_31

# SPIP 3.2
_core_/branches/spip-3.2/plugins/aide;aide_32
_core_/branches/spip-3.2/plugins/archiviste;archiviste_32
_core_/branches/spip-3.2/plugins/breves;breves_32
_core_/branches/spip-3.2/plugins/compagnon;compagnon_32
_core_/branches/spip-3.2/plugins/compresseur;compresseur_32
_core_/branches/spip-3.2/plugins/dev;dev_32
_core_/branches/spip-3.2/plugins/dist;dist_32
_core_/branches/spip-3.2/plugins/dump;dump_32
_core_/branches/spip-3.2/plugins/filtres_images;filtres_images_32
_core_/branches/spip-3.2/plugins/forum;forum_32
_core_/branches/spip-3.2/plugins/grenier;grenier_32
_core_/branches/spip-3.2/plugins/jquery_ui;jquery_ui_32
_core_/branches/spip-3.2/plugins/mediabox;mediabox_32
_core_/branches/spip-3.2/plugins/medias;medias_32
_core_/branches/spip-3.2/plugins/mots;mots_32
_core_/branches/spip-3.2/plugins/msie_compat;msie_compat_32
_core_/branches/spip-3.2/plugins/organiseur;organiseur_32
_core_/branches/spip-3.2/plugins/petitions;petitions_32
_core_/branches/spip-3.2/plugins/plan;plan_32
_core_/branches/spip-3.2/plugins/porte_plume;porte_plume_32
_core_/branches/spip-3.2/plugins/revisions;revisions_32
_core_/branches/spip-3.2/plugins/safehtml;safehtml_32
_core_/branches/spip-3.2/plugins/sites;sites_32
_core_/branches/spip-3.2/plugins/squelettes_par_rubrique;squelettes_par_rubrique_32
_core_/branches/spip-3.2/plugins/statistiques;statistiques_32
_core_/branches/spip-3.2/plugins/svp;svp_32
_core_/branches/spip-3.2/plugins/textwheel;textwheel_32
_core_/branches/spip-3.2/plugins/themes;themes_32
_core_/branches/spip-3.2/plugins/urls_etendues;urls_etendues_32
_core_/branches/spip-3.2/plugins/vertebres;vertebres_32
