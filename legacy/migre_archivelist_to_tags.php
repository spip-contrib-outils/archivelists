#!/usr/bin/php
<?php

$dir_tmp = "tmp/";
$file_archivelist = "archivelist.txt";
$organisation = "spip-contrib-themes";
$url_repo_git = "git@git.spip.net:";
$url_endpoint_api = "https://git.spip.net/api/v1/";
$file_org_repos = $dir_tmp . $organisation . ".json";


if (!is_dir($dir_tmp)) {
	mkdir($dir_tmp);
}

if (!file_exists($file_org_repos)) {
	$url_api = $url_endpoint_api . "orgs/$organisation/repos";
	passthru("curl -L --silent $url_api > $file_org_repos");
}
if (!file_exists($file_org_repos)
  or !$organisation_projets = file_get_contents($file_org_repos)
	or !$organisation_projets = json_decode($organisation_projets, true)){
	die("Impossible de recuperer la liste des projets pour $organisation");
}

$max_count = 100;
$count = 0;
$archivelist = file_get_contents($file_archivelist);
$archivelist = explode("\n", $archivelist);
foreach ($archivelist as $k => $line) {
	$line = rtrim($line);
	if (strpos($line,'#') !== 0 and strlen($line)) {
		$parts = explode(';', $line);
		$branche = 'master';
		$path = array_shift($parts);
		list($projet, $branche) = construire_projet_nom($path);
		if ($projet) {
			if ($clone_url = trouver_url_git_projet($projet)) {
				$count++;
				echo "\n\n$count. ===> $line\n";
				$dir = cloner_branche($clone_url, $branche);
				if (!$dir) {
					echo " /!\ Ignore\n";
					continue;
				}
				$version = trouver_version($dir);
				if (!$version) {
					echo "Impossible de trouver un numero de version\n";
					echo " /!\ Ignore\n";
					continue;
				}
				if (!$v = verifier_version($version)) {
					die("Version $version dans un mauvais format\n");
				}
				if (!poser_tag($dir, "v{$v}")) {
					die("Echec au moment de poser le tag v{$v} sur $dir");
				}
				// on peut retirer la ligne de l'archivelist
				unset($archivelist[$k]);
				// on ecrit le archivelist
				file_put_contents($file_archivelist, implode("\n", $archivelist));

				if ($count>=$max_count){
					break;
				}
			}
		}
	}
}

// et on re-ecrit le archivelist
file_put_contents($file_archivelist, implode("\n", $archivelist));

function construire_projet_nom($path) {
	$projet = '';
	$branche = 'master';
	if (strpos($path, '_plugins_/') === 0
		or strpos($path, '_squelettes_/') === 0){
		$projet = explode('/', $path);
		array_shift($projet);
		if (count($projet)>1){
			$branche = array_pop($projet);
			if ($branche==='trunk'){
				$branche = 'master';
			} else {
				if (end($projet)==='branches'){
					array_pop($projet);
				}
			}
		}
		$projet = implode('/', $projet);
		return [$projet, $branche];
	}
	if (strpos($path, '_themes_/') === 0){
		$path = explode('/', $path);
		array_shift($path);
		$first = array_shift($path);
		switch ($first){
			case 'dist':
			case 'spipr':
				$projet = $first . '_' . array_shift($path) . '_' . array_shift($path);
				break;

			case 'sarkaspip-3':
				$projet = 'sarkaspip_v3_';
				$dir = reset($path);
				if (strpos($dir, 'sarkaspip_') === 0) {
					$dir = substr($dir, 10);
				}
				$projet .= $dir;
				break;
			case 'zpip-2':
				$projet = 'zpip_v2_' . reset($path);
				break;

			case 'zpip-1':
				$projet = 'zpip_v1_';
				$dir = array_shift($path);
				if ($dir === 'Andreas09') {
					$dir = array_shift($path);
				}
				elseif ($dir === 'Arclite') {
					$dir = array_shift($path);
				}
				$projet .= $dir;
				break;


			default:
				$projet = $first . '_' . reset($path);
				break;
		}
var_dump($projet);
		return [$projet, $branche];
	}

	return [$projet, $branche];
}

function trouver_url_git_projet($projet) {
	foreach ($GLOBALS['organisation_projets'] as $repo) {
		if ($repo['name'] === $projet) {
			return $repo['ssh_url'];
		}
	}
	return false;
}

function cloner_branche($url, $branche) {
	$dir_dest = $GLOBALS['dir_tmp'] . basename($url, '.git');
	$command = "checkout.php -b{$branche} $url $dir_dest";
	echo "$command\n";
	passthru($command);

	// verifier qu'on est bien sur la bonne branche
	$branche_checked = "";
	$output = [];
	exec("cd $dir_dest && git status -b -s",$output);
	// ## master...origin/master
	$output = reset($output);
	if (strpos($output, '...') !== false) {
		$branche_checked = trim(substr($output,2));
		$branche_checked = explode('...', $branche_checked);
		$branche_checked = reset($branche_checked);
	}

	if ($branche_checked === $branche) {
		return "$dir_dest/";
	}
	echo "/!\ pas de branche $branche\n";
	return false;
}

function trouver_version($dir) {
	if (file_exists($f = $dir . "paquet.xml")) {
		$xml = file_get_contents($f);
		if (preg_match(",<paquet\b.*version=['\"]([\d.]+)['\"],ims", $xml, $m)){
			$version = $m[1];
			return $version;
		}
	}
	if (file_exists($f = $dir . "plugin.xml")) {
		$xml = file_get_contents($f);
		if (preg_match(",<version>\s*([\d.]+)\s*</version>,ims", $xml, $m)){
			$version = $m[1];
			return $version;
		}
	}

	return false;
}

function verifier_version($version) {
	$version = explode('.', $version);
	if (count($version) == 3 or count($version) == 4) {
		return implode('.', $version);
	}
	return false;
}

function poser_tag($dir, $tagname) {
	$command = "cd $dir && git tag $tagname";
	echo "$command\n";
	passthru($command);

	// verifier que le tag est la
	$command = "cd $dir && git tag -l";
	$output = [];
	exec($command, $output);
	$ok = false;
	foreach ($output as $tag) {
		if ($tag === $tagname) {
			$ok = true;
			break;
		}
	}
	if (!$ok) {
		return false;
	}

	$command = "cd $dir && git push --tags";
	echo "$command\n";
	passthru($command);
	return true;
}
